import { getSignList ,getSignDetail,getAddSign } from "@/services";

const state = {
    signList: [], 
    signDetail:{},
    addSign:{}
}

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
}

const actions = {
    // 数据列表
    async getSignList({ commit, state } ,payload){  
        let res = await getSignList(payload);
        console.log("面试列表",res.data); 
        if (res.code === 0){
        //   try{
        //     res.data.forEach((item,index)=>{
        //         item.address =  JSON.parse(item.address)
        //         item.address.location.lat = item.address.location.lat*1
        //         item.address =  JSON.stringify(item.address)
        //     })
        //     console.log(JSON.parse(res.data[0].address).location.lat);
        //     console.log(23262165135135153135135);
                 
        //   }catch (e){
        //       console.log(11111111111);
        //       res.data.forEach((item,index)=>{
        //           console.log(item.address,"+++++++++");
        //         item.address = JSON.parse(item.address)
        //         item.addresss =JSON.stringify({
        //             "id":item.address.form_id,
        //             "title":item.address.company,
        //             "address":item.address.address.address,
        //             "tel":item.address.phone,
        //             "category":"",
        //             "type":0,
        //             "location":{
        //                 "lat":item.address.latitude,
        //                 "lng":item.address.longitude
        //             },
        //             "_distance":1755.5,
        //         })
        //       })
             
        //   }
            if (payload.page && payload.page > 1){
                signList = [...state.signList, ...res.data];
            }
            commit('update', {
                signList:res.data, 
            })
        }
    },
    // 详情
    async getSignDetail({ commit }, payload){ 
        console.log("payload...",payload);
        let res = await getSignDetail(payload);
        console.log("详情...",res.data); 
        try{
            
            console.log(JSON.parse(res.data.address).location.lat);
            console.log(23262165135135153135135);
                 
          }catch (e){
              let address = JSON.parse(res.data.address)
              res.data.address =JSON.stringify({
                "id":address.form_id,
                "title":address.company,
                "address":address.address.address,
                "tel":address.phone,
                "category":"",
                "type":0,
                "location":{
                    "lat":address.latitude,
                    "lng":address.longitude
                },
                "_distance":1755.5,
            })
            console.log(res.data,1111111111111);
          }
        if (res.code === 0){
            commit('update', {
                signDetail: res.data
            });
        }
    },
    async getAddSign({ commit },payload){
        console.log("参数",payload);
        let result = await getAddSign(payload);
        console.log("添加.....",result);
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}