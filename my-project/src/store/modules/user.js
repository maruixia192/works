import { userLogin } from "@/services";

const state = {
    userInfo: {},
    isLogin: false
}

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
}

const actions = {
    async userLogin({commit}, payload){ 
        let result = await userLogin(payload);
        console.log("登录result",result.data); 
        if (result.code === 0){
            commit('update', {
                userInfo: result.data,
                isLogin: true
            })
            // 把登陆态存储到本地
            wx.setStorageSync("tel",result.data.phone)
            wx.setStorageSync('openid', result.data.openid);
        }
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}