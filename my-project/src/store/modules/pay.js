import { pays } from "@/services";

const state = {
    payData: [], 
}

const mutations = {
    update(state, payload){
        for (let key in payload) {
            state[key] = payload[key];
        }
    }
}

const actions = {
    // 数据列表
    async pays({ commit } ,payload){  
        console.log("payload",payload);
        let result = await pays(payload);
        // console.log("支付。。",result.data); 
        if (result.code === 0){ 
            commit('update', {
                payData: result.data, 
            })
        }
    } 
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}