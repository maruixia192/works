import Vuex from 'vuex';
import Vue from 'vue';
import createLogger from 'vuex/dist/logger'
 
// 引入子模块
import user from './modules/user'
import sign from './modules/sign'
import pay from './modules/pay'
 
Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user, 
        sign,
        pay
    },
    plugins: [createLogger()]
});