import Vue from 'vue'
import App from './App'
// 引入vuex
import store from '@/store'
import uView from "uview-ui";
import VueCompositionAPI from '@vue/composition-api'

Vue.use(uView);
Vue.use(VueCompositionAPI)

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
  store,
  ...App
})
app.$mount()
