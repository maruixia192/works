import request from "@/utils/request"

//用户登录
export function getSignList(data){
    return request.get("/sign",data)
}
// 获取面试详情
export function getSignDetail(id){
    return request.get(`/sign/${id}`)
}

//添加面试
export function getAddSign(data){
    console.log("传过来的数据",data);//这个都不打印
    return request.post("/sign",data)
}